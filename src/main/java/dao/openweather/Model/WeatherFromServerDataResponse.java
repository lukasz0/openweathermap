package dao.openweather.Model;

import okhttp3.*;

import java.io.IOException;


public class WeatherFromServerDataResponse {

    public static final String WEATHER_URL = "http://api.openweathermap.org/data/2.5/weather";

    public  String getWeather(){
        HttpUrl.Builder urlBuilder = HttpUrl.parse(WEATHER_URL).newBuilder();
        urlBuilder.addQueryParameter("q","Warsaw");
        urlBuilder.addQueryParameter("appid","8296fc0b0a4a4c127a6bf58432026042");


        String url = urlBuilder.build().toString();

        Request request=new Request.Builder()
                .url(url)
                .build();
        OkHttpClient client = new OkHttpClient();
        Call call = client.newCall(request);
        try {
            Response response= call.execute();
            System.out.println(response.body().string());
            return  response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


}
