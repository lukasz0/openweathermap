package cli;

import Conroller.WeathermanController;
import Conroller.WeatherRequestModel;
import model.LocationModel;
import model.WeatherModel;


import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class WeathermanCli {
    private final Scanner scanner;
    private final WeathermanController weathermanController = new WeathermanController();
    private static final Logger LOGGER = Logger.getLogger(WeathermanCli.class.getName());


    public WeathermanCli(Scanner scanner) {
        this.scanner = scanner;
    }

    public void mainLoop() {
        System.out.println("1. Lista dostępnych lokalizacji.");
        System.out.println("2. Tworzenie nowej lokalizacji bez aktualnej pogody.");
        System.out.println("3. Tworzenie nowej lokalizacji z aktualną pogodą.");
        System.out.println("4. Aktualna pogoda dla istniejącej lokalizacji.");
        System.out.println("5. Aktualizuj lokalizację.");
        System.out.println("6. Usuń lokalizacją z bazy danych.");
        System.out.println("7. Usuń lokalizację za bazy danych wyszukaną po nr id");
        System.out.println("8. Aktualna oraz wszystkie dostępne pogody dla istniejącej lokalizacji.");
        System.out.println("9. Zakończ program.");
        int i = 0;

        do {
            try {
                i = Integer.parseInt(scanner.nextLine());
            } catch (NumberFormatException e) {
                LOGGER.log(Level.WARNING, e.getMessage());
                e.printStackTrace();
            }
            switch (i) {
                case 1:
                    System.out.println("Lista dostępnych lokalizacji");
                    weathermanController.listLocation().forEach(System.out::println);
                    break;
                case 2:
                    System.out.println("Tworzenie nowej lokalizacji bez aktualnej pogody: \nPodaj nazwę miasta");
                    String city = validator(scanner);
                    System.out.println("Podaj countrycode dla miasta " + city);
                    String countryCode = validator(scanner);
                    WeatherRequestModel weatherRequestModel = new WeatherRequestModel(city, countryCode);
                    LocationModel location = weathermanController.createLocation(weatherRequestModel);
                    System.out.println("Utworzono nową lokalizację " + location);

                    break;
                case 3:
                    System.out.println("Tworzenie nowej lokalizacji z aktualną pogodą. \n Podaje nazwę miasta");
                    city = validator(scanner);
                    System.out.println("Podaj countrycode dla miasta " + city);
                    countryCode = validator(scanner);
                    weatherRequestModel = new WeatherRequestModel(city, countryCode);
                    WeatherModel weatherModel = weathermanController.createLocationWithWeather(weatherRequestModel);
                    LocationModel locationModel = weathermanController.read(weatherRequestModel);
                    System.out.println("Utworzono nową lokalizację  " + locationModel);
                    System.out.println("z aktualną pogodą " + weatherModel);

                    break;
                case 4:
                    System.out.println("Aktualna pogoda dla istniejącej lokalizacji. \n Podaje nazwę miasta");
                    city = validator(scanner);
                    System.out.println("Podaj countrycode dla miasta " + city);
                    countryCode = validator(scanner);
                    weatherRequestModel = new WeatherRequestModel(city, countryCode);
                    LocationModel weatherForLocation = weathermanController.createWeatherForLocation(weatherRequestModel);
                    locationModel = weathermanController.readLastWeather(weatherRequestModel);
                    System.out.println("Aktualna pogoda dla " + locationModel);
                    break;
                case 5:
                    System.out.println("Aktualizuj lokalizację. \n Podaje nazwę miasta");
                    city = validator(scanner);
                    System.out.println("Podaj countrycode dla miasta " + city);
                    countryCode = validator(scanner);
                    weatherRequestModel = new WeatherRequestModel(city, countryCode);
                    System.out.println("Podaj id lokalizacji do aktualizacji");
                    Long id = null;
                    do {
                        try {
                            id = Long.parseLong(scanner.nextLine());
                        } catch (NumberFormatException e) {
                            LOGGER.log(Level.WARNING, e.getMessage());
                            e.printStackTrace();
                        }
                    } while (id == null);

                    LocationModel updateLocationById = weathermanController.updateLocationById(id, weatherRequestModel);
                    System.out.println("Lokalizacja o nr id " + id + " po akutalizacji to " + updateLocationById);
                    break;
                case 6:
                    System.out.println("Usuń lokalizacją z bazy danych.\n Podaje nazwę miasta");
                    city = validator(scanner);
                    System.out.println("Podaj countrycode dla miasta " + city);
                    countryCode = validator(scanner);
                    weatherRequestModel = new WeatherRequestModel(city, countryCode);
                    LocationModel deleteLocation = weathermanController.deleteLocation(weatherRequestModel);
                    System.out.println("Usunięto lokalizację z bazy danych " + deleteLocation);
                    break;
                case 7:
                    System.out.println("Usuń lokalizację za bazy danych wyszukaną po nr id. \n Podaj nr id lokalizacji");
                    id = null;
                    do {
                        try {
                            id = Long.parseLong(scanner.nextLine());
                        } catch (NumberFormatException e) {
                            LOGGER.log(Level.WARNING, e.getMessage());
                            e.printStackTrace();
                        }
                    } while (id == null);
                    LocationModel deleteLocationById = weathermanController.deleteLocationById(id);
                    System.out.println("Usunięto lokalizację z bazy danych " + deleteLocationById);
                    break;
                case 8:
                    System.out.println("Aktualna oraz wszystkie dostępne pogody dla istniejącej lokalizacji. \n Podaje nazwę miasta");
                    city = validator(scanner);
                    System.out.println("Podaj countrycode dla miasta " + city);
                    countryCode = validator(scanner);
                    weatherRequestModel = new WeatherRequestModel(city, countryCode);
                    weatherForLocation = weathermanController.createWeatherForLocation(weatherRequestModel);
                    locationModel = weathermanController.readLastWeather(weatherRequestModel);
                    System.out.println("Aktualna pogoda dla " + locationModel);
                    System.out.println(weatherForLocation);
                    break;
                case 9:
                    System.out.println();
                    break;
                default:
                    System.out.println("Podaj liczbę z zakresu od 1 do 9 \n");
            }
            if (i!=9){
                System.out.println("1. Lista dostępnych lokalizacji.");
                System.out.println("2. Tworzenie nowej lokalizacji bez aktualnej pogody.");
                System.out.println("3. Tworzenie nowej lokalizacji z aktualną pogodą.");
                System.out.println("4. Aktualna pogoda dla istniejącej lokalizacji.");
                System.out.println("5. Aktualizuj lokalizację.");
                System.out.println("6. Usuń lokalizacją z bazy danych.");
                System.out.println("7. Usuń lokalizację za bazy danych wyszukaną po nr id");
                System.out.println("8. Aktualna oraz wszystkie dostępne pogody dla istniejącej lokalizacji.");
                System.out.println("9. Zakończ program.");
            }

        } while (i != 9);


        //while(true)
        //switch (command)


//        V-view
//        String city=scanner.nextLine();
//        String countryCode=scanner.nextLine();
//
//        //M - model
//
//        WeatherRequestModel weatherRequestModel=new WeatherRequestModel(city,countryCode);
//
//        // C controller
//        WeathermanController weathermanController =new WeathermanController();
//        weathermanController.createLocation(weatherRequestModel);

    }

    private static String validator(Scanner scanner) {
        String pattern = "([A-Z,-])([A-Z,a-z,ą,ę,ć,ń,ó,ś,ł,ż,ź,-, ]+)";
        boolean b;
        String text = null;
        do {
            b = scanner.hasNext(pattern);
            text = scanner.nextLine();
            if (!b) {
                System.out.println("Podaj prawidłową nazwę");
            } else {
                text = text.trim();
            }
        } while (!b);
        return text;
    }

}
