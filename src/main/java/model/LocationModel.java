package model;


import java.util.ArrayList;
import java.util.List;

public class LocationModel {

    private Long id;
    private String city;
    private double latitude;
    private double longitude;
    private String region;
    private String country;

    private List<WeatherModel> weatherModels = new ArrayList<>();

//    public LocationModel() {
//    }

    public LocationModel(Long id, String city, double latitude, double longitude, String region, String country, List<WeatherModel> weatherModels) {
        this.id = id;
        this.city = city;
        this.latitude = latitude;
        this.longitude = longitude;
        this.region = region;
        this.country = country;
        this.weatherModels = weatherModels;
    }

    public Long getId() {
        return id;
    }

    public LocationModel setId(Long id) {
        this.id = id;
        return this;
    }

    public String getCity() {
        return city;
    }

    public LocationModel setCity(String city) {
        this.city = city;
        return this;
    }

    public double getLatitude() {
        return latitude;
    }

    public LocationModel setLatitude(double latitude) {
        this.latitude = latitude;
        return this;
    }

    public double getLongitude() {
        return longitude;
    }

    public LocationModel setLongitude(double longitude) {
        this.longitude = longitude;
        return this;
    }

    public String getRegion() {
        return region;
    }

    public LocationModel setRegion(String region) {
        this.region = region;
        return this;
    }

    public String getCountry() {
        return country;
    }

    public LocationModel setCountry(String country) {
        this.country = country;
        return this;
    }

    public List<WeatherModel> getWeatherModels() {
        return weatherModels;
    }

    public LocationModel setWeatherModels(List<WeatherModel> weatherModels) {
        this.weatherModels = weatherModels;
        return this;
    }

    @Override
    public String toString() {
        return "LocationModel{" +
                "id=" + id +
                ", city='" + city + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", region='" + region + '\'' +
                ", country='" + country + '\'' +
                ", weatherModels=" + weatherModels +
                '}';
    }
}
