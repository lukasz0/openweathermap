package model;


public class WeatherModel {

    private Long idWeather;

    private double temperature;
    private Integer pressure; //ciśnienie
    private Integer humidity; //wilgotność
    private Double windSpeed;
    private Integer windDegrees;

    private LocationModel locationModel;

    public LocationModel getLocationModel() {
        return locationModel;
    }

    public WeatherModel setLocationModel(LocationModel locationModel) {
        this.locationModel = locationModel;
        return this;
    }

//    public WeatherModel() {
//    }
//
//    public WeatherModel(double temperature, Integer pressure, Integer humidity, Double windSpeed, Integer windDegrees) {
//        this.temperature = temperature;
//        this.pressure = pressure;
//        this.humidity = humidity;
//        this.windSpeed = windSpeed;
//        this.windDegrees = windDegrees;
//    }
//
//    public WeatherModel(Long idWeather, double temperature, Integer pressure, Integer humidity, Double windSpeed, Integer windDegrees) {
//        this.idWeather = idWeather;
//        this.temperature = temperature;
//        this.pressure = pressure;
//        this.humidity = humidity;
//        this.windSpeed = windSpeed;
//        this.windDegrees = windDegrees;
//    }

    public WeatherModel(Long idWeather, double temperature, Integer pressure, Integer humidity, Double windSpeed, Integer windDegrees, LocationModel locationModel) {
        this.idWeather = idWeather;
        this.temperature = temperature;
        this.pressure = pressure;
        this.humidity = humidity;
        this.windSpeed = windSpeed;
        this.windDegrees = windDegrees;
        this.locationModel = locationModel;
    }

    public Long getIdWeather() {
        return idWeather;
    }

    public WeatherModel setIdWeather(Long idWeather) {
        this.idWeather = idWeather;
        return this;
    }

    public double getTemperature() {
        return temperature;
    }

    public WeatherModel setTemperature(double temperature) {
        this.temperature = temperature;
        return this;
    }

    public Integer getPressure() {
        return pressure;
    }

    public WeatherModel setPressure(Integer pressure) {
        this.pressure = pressure;
        return this;
    }

    public Integer getHumidity() {
        return humidity;
    }

    public WeatherModel setHumidity(Integer humidity) {
        this.humidity = humidity;
        return this;
    }

    public Double getWindSpeed() {
        return windSpeed;
    }

    public WeatherModel setWindSpeed(Double windSpeed) {
        this.windSpeed = windSpeed;
        return this;
    }

    public Integer getWindDegrees() {
        return windDegrees;
    }

    public WeatherModel setWindDegrees(Integer windDegrees) {
        this.windDegrees = windDegrees;
        return this;
    }

    @Override
    public String toString() {
        return "Weather{" +
                "idWeather=" + idWeather +
                ", temperature=" + temperature +
                ", pressure=" + pressure +
                ", humidity=" + humidity +
                ", windSpeed=" + windSpeed +
                ", windDegrees=" + windDegrees +
                '}';
    }
}
