package model;

import model.modelEntity.LocationEntity;
import model.modelEntity.WeatherEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
//import org.hibernate.Transaction;
//import org.hibernate.boot.MetadataSources;
//import org.hibernate.boot.SessionFactoryBuilder;
//import org.hibernate.boot.registry.StandardServiceRegistry;
//import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
//import org.hibernate.boot.spi.SessionFactoryBuilderFactory;

import java.io.Serializable;
import java.util.List;
//import java.lang.module.Configuration;

public class HibernateWeathermanDao {

    public LocationEntity createDao(LocationEntity locationEntity) {

//        connectDb();

        SessionFactory sessionFactory = HibernateUtil.getInstance().getSessionFactory();

        Session session=null;
        LocationEntity foundLocationEntity = null;

        try{

            session.getTransaction().begin();
            Serializable saveLocationEntity = session.save(locationEntity);
            session.getTransaction().commit();

            session.getTransaction().begin();
            foundLocationEntity = session.find(LocationEntity.class, saveLocationEntity);
            session.getTransaction().commit();
        } catch (Exception e){
            if (session != null) {
                session.getTransaction().rollback();
            }
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return foundLocationEntity;

//        if (sessionFactory != null) {
//            sessionFactory.close(); //zamknąć przy kończeniu programu

//        }
//        TODO: zamknąć sessionFactory - sessionFactory.close()
    }

    //dopisać bloki try catch i ewentualnie finnaly

    public WeatherEntity createWeatherWithLocationDao(WeatherEntity weatherEntity) {
        SessionFactory sessionFactory = HibernateUtil.getInstance().getSessionFactory();


        Session session = sessionFactory.openSession();
        session.getTransaction().begin();
        Serializable save = session.save(weatherEntity);
        session.getTransaction().commit();
        session.close();
        session = sessionFactory.openSession();
        session.getTransaction().begin();
        WeatherEntity foundWeatherEntity = session.find(WeatherEntity.class, save);
        session.getTransaction().commit();
        session.close();
        return foundWeatherEntity;
    }

    //R

    public LocationEntity readDao(LocationModel locationModel) {
        SessionFactory sessionFactory = HibernateUtil.getInstance().getSessionFactory();
        Session session = sessionFactory.openSession();
        session.getTransaction().begin();
        Query<LocationEntity> query = session.createQuery("FROM LocationEntity WHERE city = '" + locationModel.getCity() + "'", LocationEntity.class);
        List<LocationEntity> querySingleResult = query.getResultList();
        session.getTransaction().commit();
        session.close();
        if (querySingleResult.isEmpty()) {
            return null;
        }
        LocationEntity locationEntity = querySingleResult.get(0);
        return locationEntity;
    }

    public LocationEntity findById(Long idLocation) {
        SessionFactory sessionFactory = HibernateUtil.getInstance().getSessionFactory();
        Session session = sessionFactory.openSession();
        session.getTransaction().begin();
        LocationEntity locationEntityOut = session.find(LocationEntity.class, idLocation);
        session.getTransaction().commit();
        session.close();
        return locationEntityOut;
    }


    //U

    public LocationEntity updateLocationDao(LocationEntity locationEntity) {
        SessionFactory sessionFactory = HibernateUtil.getInstance().getSessionFactory();
        Session session = sessionFactory.openSession();
        session.getTransaction().begin();
        LocationEntity locationEntityOut = (LocationEntity) session.merge(locationEntity);
        session.getTransaction().commit();
        session.close();
        return locationEntityOut;
    }

    //D


    public void deleteLocationDAO(LocationEntity locationEntity) {

        SessionFactory sessionFactory = HibernateUtil.getInstance().getSessionFactory();
        Session session = sessionFactory.openSession();
        session.getTransaction().begin();
        session.remove(locationEntity);
        session.getTransaction().commit();
        session.close();
    }


    //L

    public static List<LocationEntity> getLocationEntities() {
        SessionFactory sessionFactory = HibernateUtil.getInstance().getSessionFactory();
        Session session = sessionFactory.openSession();
        session.getTransaction().begin();
        Query<LocationEntity> locationEntity = session.createQuery("From LocationEntity", LocationEntity.class);
        List<LocationEntity> locationEntityResultList = locationEntity.getResultList();
        session.getTransaction().commit();
        session.close();
        return locationEntityResultList;
    }
}
