package model;

import Conroller.WeatherRequestModel;
import dao.openweather.Model.OpenWeatherApiCurrentWeatherDataResponse;
import model.modelEntity.LocationEntity;
import model.modelEntity.WeatherEntity;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Mapper {


    public LocationModel mapperFromDataResponseToLocation(OpenWeatherApiCurrentWeatherDataResponse openWeatherApiCurrentWeatherDataResponse) {
        if (openWeatherApiCurrentWeatherDataResponse == null) {
            return null;
        }

        LocationModel locationModel = new LocationModelBuilder().createLocationModel();
        locationModel.setCity(openWeatherApiCurrentWeatherDataResponse.getName());
        locationModel.setLatitude(openWeatherApiCurrentWeatherDataResponse.getCoord().getLat());
        locationModel.setLongitude(openWeatherApiCurrentWeatherDataResponse.getCoord().getLon());
        locationModel.setRegion(openWeatherApiCurrentWeatherDataResponse.getName());
        locationModel.setCountry(openWeatherApiCurrentWeatherDataResponse.getSys().getCountry());

        return locationModel;
    }

    public WeatherModel mapperFromDataResponseToWeather(OpenWeatherApiCurrentWeatherDataResponse openWeatherApiCurrentWeatherDataResponse) {
        WeatherModel weatherModel = new WeatherModelBuilder().createWeatherModel();
        weatherModel.setTemperature(openWeatherApiCurrentWeatherDataResponse.getMain().getTemp());
        weatherModel.setPressure(openWeatherApiCurrentWeatherDataResponse.getMain().getPressure());
        weatherModel.setHumidity(openWeatherApiCurrentWeatherDataResponse.getMain().getHumidity());
        weatherModel.setWindSpeed(openWeatherApiCurrentWeatherDataResponse.getWind().getSpeed());
        weatherModel.setWindDegrees(openWeatherApiCurrentWeatherDataResponse.getWind().getDeg());

        return weatherModel;
    }

    public LocationModel mapperFromWeatherRequestToLocationModel(@NotNull WeatherRequestModel weatherRequestModel) {
        LocationModel locationModel = new LocationModelBuilder().createLocationModel();
        locationModel.setCity(weatherRequestModel.getCity());
        locationModel.setCountry(weatherRequestModel.getCountryCode());

        return locationModel;

    }

    public LocationEntity mapperFromLocationModelToLocationEntity(LocationModel locationModel) {
        if (locationModel.getId() == null & locationModel.getWeatherModels() == null) {
            LocationEntity locationEntity = new LocationEntity();
            locationEntity.setCity(locationModel.getCity());
            locationEntity.setLatitude(locationModel.getLatitude());
            locationEntity.setLongitude(locationModel.getLongitude());
            locationEntity.setRegion(locationModel.getRegion());
            locationEntity.setCountry(locationModel.getCountry());
            return locationEntity;
        }

        if (locationModel.getId() == null) {
            LocationEntity locationEntity = new LocationEntity();
            locationEntity.setCity(locationModel.getCity());
            locationEntity.setLatitude(locationModel.getLatitude());
            locationEntity.setLongitude(locationModel.getLongitude());
            locationEntity.setRegion(locationModel.getRegion());
            locationEntity.setCountry(locationModel.getCountry());
            locationEntity.setWeatherEntities(mapperFromListLocationModelToListLocationEntity(locationModel.getWeatherModels()));

            return locationEntity;
        }

        if (locationModel.getWeatherModels() == null) {
            LocationEntity locationEntity = new LocationEntity();
            locationEntity.setCity(locationModel.getCity());
            locationEntity.setLatitude(locationModel.getLatitude());
            locationEntity.setLongitude(locationModel.getLongitude());
            locationEntity.setRegion(locationModel.getRegion());
            locationEntity.setCountry(locationModel.getCountry());
            return locationEntity;
        }

        LocationEntity locationEntity = new LocationEntity();
        locationEntity.setId(locationModel.getId());
        locationEntity.setCity(locationModel.getCity());
        locationEntity.setLatitude(locationModel.getLatitude());
        locationEntity.setLongitude(locationModel.getLongitude());
        locationEntity.setRegion(locationModel.getRegion());
        locationEntity.setCountry(locationModel.getCountry());
        locationEntity.setWeatherEntities(mapperFromListLocationModelToListLocationEntity(locationModel.getWeatherModels()));

        return locationEntity;
    }

    private List<WeatherEntity> mapperFromListLocationModelToListLocationEntity(List<WeatherModel> weatherModels) {
        List<WeatherEntity> weatherEntities = new ArrayList<>();
        weatherEntities = weatherModels.stream()
                .map(weatherModel -> mapperFromWeatherModelToWeatherEntity(weatherModel))
                .collect(Collectors.toList());
        return weatherEntities;
    }

    public LocationModel fromLocationEntityToLocationModel(LocationEntity locationEntity) {
        return new LocationModelBuilder()
                .setId(locationEntity.getId())
                .setCity(locationEntity.getCity())
                .setLatitude(locationEntity.getLatitude())
                .setLongitude(locationEntity.getLongitude())
                .setRegion(locationEntity.getRegion())
                .setCountry(locationEntity.getCountry())
                .setWeatherModels(fromListWeatherEntitiesToListWeatherModels(locationEntity.getWeatherEntities()))
                .createLocationModel();

    }

    private List<WeatherModel> fromListWeatherEntitiesToListWeatherModels(List<WeatherEntity> weatherEntities) {
        List<WeatherModel> weatherModels = new ArrayList<>();

        weatherModels = weatherEntities.stream()
                .map(this::fromWeatherEntityToWeatherModel)
                .collect(Collectors.toList());
        return weatherModels;
    }

    public WeatherModel fromWeatherEntityToWeatherModel(WeatherEntity weatherEntity) {
        return new WeatherModelBuilder()
                .setIdWeather(weatherEntity.getIdWeather())
                .setTemperature(weatherEntity.getTemperature())
                .setPressure(weatherEntity.getPressure())
                .setHumidity(weatherEntity.getHumidity())
                .setWindSpeed(weatherEntity.getWindSpeed())
                .setWindDegrees(weatherEntity.getWindDegrees())
                .createWeatherModel();
        //nie ustawiliśmy pola setLocationModel

    }

    public WeatherEntity mapperFromWeatherModelToWeatherEntity(WeatherModel weatherModel) {
        WeatherEntity weatherEntity = new WeatherEntity();
        weatherEntity.setTemperature(weatherModel.getTemperature());
        weatherEntity.setPressure(weatherModel.getPressure());
        weatherEntity.setHumidity(weatherModel.getHumidity());
        weatherEntity.setWindSpeed(weatherModel.getWindSpeed());
        weatherEntity.setWindDegrees(weatherModel.getWindDegrees());
        return weatherEntity;

        //nie ustawiliśmy pola setLocationEntity
    }
}
