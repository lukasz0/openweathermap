package model;

import dao.openweather.Model.OpenWeatherApiCurrentWeatherDataResponse;
import dao.openweather.Service.OpenWeatherApiCurrentWeatherDataService;
import model.modelEntity.LocationEntity;
import model.modelEntity.WeatherEntity;

import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class WeathermanService {

    public static final Logger LOGGER = Logger.getLogger(WeathermanService.class.getName());
    private final HibernateWeathermanDao hibernateWeathermanDao = new HibernateWeathermanDao();
    private final OpenWeatherApiCurrentWeatherDataService openWeatherApiCurrentWeatherDataService = new OpenWeatherApiCurrentWeatherDataService();

    Mapper mapper = new Mapper();

    //c
    public LocationModel createLocation(LocationModel locationModel) {
        LocationEntity locationEntity = mapper.mapperFromLocationModelToLocationEntity(locationModel);
        if (read(locationModel) != null) {
            locationModel = read(locationModel);
            LOGGER.info("lokalizacja już istnieje");
            return locationModel;
        }
        if (locationEntity == null) {
            LOGGER.info("LocationEntity jest null");
            return locationModel;
        }

        //create in database
        locationEntity = hibernateWeathermanDao.createDao(locationEntity);
        LOGGER.info("locationEntity z BD" + locationEntity);
        locationModel = mapper.fromLocationEntityToLocationModel(locationEntity);
        return locationModel;
    }

    public WeatherModel createWeatherWithLocation(LocationModel locationModel) {

        LocationModel readLocationModelFromDB = read(locationModel);
        if (readLocationModelFromDB != null) {
            locationModel = readLocationModelFromDB;
            LOGGER.info("Nie można dodać lokalizacji z pogodą, gdyż lokalizacja już istnieje");
            return null;
        }

        OpenWeatherApiCurrentWeatherDataResponse currentWeather = openWeatherApiCurrentWeatherDataService.getCurrentWeather(locationModel.getCity());
        LocationModel locationModelFromOpenWeather =mapper.mapperFromDataResponseToLocation(currentWeather);
        LOGGER.info("Po zmianach z weather APi" +locationModelFromOpenWeather);

        WeatherModel weatherModel = mapper.mapperFromDataResponseToWeather(currentWeather);
        WeatherEntity weatherEntity = mapper.mapperFromWeatherModelToWeatherEntity(weatherModel);
        weatherEntity.setLocationEntity(mapper.mapperFromLocationModelToLocationEntity(locationModelFromOpenWeather));

        //create in database
        weatherEntity = hibernateWeathermanDao.createWeatherWithLocationDao(weatherEntity);
        weatherModel = mapper.fromWeatherEntityToWeatherModel(weatherEntity);
        return weatherModel;
    }

    public LocationModel createWeatherForLocation(LocationModel locationModel) {
        if (read(locationModel) == null) {
            LOGGER.info("Nie można dodać pogody bo nie ma takiej lokalizacji w DB. Wybierz opcję dodaj lokalizacje z pogoda");
            return null;
        }
        locationModel = read(locationModel);
        OpenWeatherApiCurrentWeatherDataResponse currentWeather = openWeatherApiCurrentWeatherDataService.getCurrentWeather(locationModel.getCity());
        WeatherModel weatherModel = mapper.mapperFromDataResponseToWeather(currentWeather);

        LocationModel locationModelFromOpenWeather=mapper.mapperFromDataResponseToLocation(currentWeather);

        //uzupelniamy lokalizacje o dane z openWeather
        locationModel.setLongitude(locationModelFromOpenWeather.getLongitude());
        locationModel.setLatitude(locationModelFromOpenWeather.getLatitude());
        locationModel.setRegion(locationModelFromOpenWeather.getRegion());
        locationModel.setCountry(locationModelFromOpenWeather.getCountry());

        List<WeatherModel> weatherModels = locationModel.getWeatherModels();

        weatherModels.add(weatherModel);
        locationModel.setWeatherModels(weatherModels);

        LOGGER.info("locationModel po dodaniu kolejnej pogody" + locationModel);
        LocationEntity locationEntity = mapper.mapperFromLocationModelToLocationEntity(locationModel);

        locationEntity.getWeatherEntities()
                .forEach(weatherEntity -> weatherEntity.setLocationEntity(locationEntity));

        WeatherEntity weatherEntity = mapper.mapperFromWeatherModelToWeatherEntity(weatherModel);
        weatherEntity.setLocationEntity(locationEntity);

        hibernateWeathermanDao.updateLocationDao(locationEntity);
        return locationModel;

    }

    //r

    public LocationModel read(LocationModel locationModel) {

        LocationEntity locationEntity = hibernateWeathermanDao.readDao(locationModel);
        if (locationEntity == null) {
            LOGGER.info("LocationEntity is null");
            return null;
        }
        LocationModel locationModelOut = mapper.fromLocationEntityToLocationModel(locationEntity);
        return locationModelOut;
    }

    //u
    public LocationModel updateLocationService(Long idLocation, LocationModel locationModel) {
        LocationModel locationModelById = findById(idLocation);
        if (locationModelById == null) {
            LOGGER.info("Nie można aktualizować lokalizacji bo nie ma takiej lokalizacji w DB. Wybierz opcję dodaj lokalizacje");
            return null;
        }
        LocationEntity locationEntity = mapper.mapperFromLocationModelToLocationEntity(locationModel);
        locationEntity.setId(idLocation);
        LocationEntity locationEntityOut = hibernateWeathermanDao.updateLocationDao(locationEntity);
        return mapper.fromLocationEntityToLocationModel(locationEntityOut);
    }

    private LocationModel findById(Long idLocation) {
        LocationEntity locationEntityOut = hibernateWeathermanDao.findById(idLocation);
        return mapper.fromLocationEntityToLocationModel(locationEntityOut);
    }

    //d

    public LocationModel deleteLocationService(LocationModel locationModel) {

        LocationEntity locationEntityFromDB = hibernateWeathermanDao.readDao(locationModel);
        if (locationEntityFromDB == null) {
            LOGGER.info("Nie można usunąć lokalizacji, bo nie ma takiej lokalizacji w DB.");
            return null;
        }
        hibernateWeathermanDao.deleteLocationDAO(locationEntityFromDB);
        LocationModel locationModelOut = mapper.fromLocationEntityToLocationModel(locationEntityFromDB);
        return locationModelOut;
    }


    public LocationModel deleteLocationByIdService(Long idLocation) {
        LocationEntity locationEntityFromDB = hibernateWeathermanDao.findById(idLocation);
        if (locationEntityFromDB == null) {
            LOGGER.info("Nie można usunąć lokalizacji, bo nie ma takiej lokalizacji w DB.");
            return null;
        }
        hibernateWeathermanDao.deleteLocationDAO(locationEntityFromDB);
        LocationModel locationModelOut = mapper.fromLocationEntityToLocationModel(locationEntityFromDB);
        return locationModelOut;
    }


    //l
    public List<LocationModel> getLocationModels() {
        List<LocationEntity> locationEntities = HibernateWeathermanDao.getLocationEntities();
        List<LocationModel> locationModels = locationEntities.stream()
                .map(locationEntity -> mapper.fromLocationEntityToLocationModel(locationEntity))
                .collect(Collectors.toList());
        return locationModels;

    }
}