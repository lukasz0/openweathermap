package model.modelEntity;


import javax.persistence.*;

@Entity//(name ="Weathers")
public class WeatherEntity {
    @Id
    @GeneratedValue
    private Long idWeather;

    private double temperature;
    private Integer pressure; //ciśnienie
    private Integer humidity; //wilgotność
    private Double windSpeed;
    private Integer windDegrees;


    @ManyToOne(cascade = CascadeType.ALL)
    private LocationEntity locationEntity;

    public WeatherEntity() {
    }

    public WeatherEntity(double temperature, Integer pressure, Integer humidity, Double windSpeed, Integer windDegrees) {
        this.temperature = temperature;
        this.pressure = pressure;
        this.humidity = humidity;
        this.windSpeed = windSpeed;
        this.windDegrees = windDegrees;
    }

    public WeatherEntity(Long idWeather, double temperature, Integer pressure, Integer humidity, Double windSpeed, Integer windDegrees) {
        this.idWeather = idWeather;
        this.temperature = temperature;
        this.pressure = pressure;
        this.humidity = humidity;
        this.windSpeed = windSpeed;
        this.windDegrees = windDegrees;
    }

    public WeatherEntity(Long idWeather, double temperature, Integer pressure, Integer humidity, Double windSpeed, Integer windDegrees, LocationEntity locationEntity) {
        this.idWeather = idWeather;
        this.temperature = temperature;
        this.pressure = pressure;
        this.humidity = humidity;
        this.windSpeed = windSpeed;
        this.windDegrees = windDegrees;
        this.locationEntity = locationEntity;
    }

    public Long getIdWeather() {
        return idWeather;
    }

    public WeatherEntity setIdWeather(Long idWeather) {
        this.idWeather = idWeather;
        return this;
    }

    public double getTemperature() {
        return temperature;
    }

    public WeatherEntity setTemperature(double temperature) {
        this.temperature = temperature;
        return this;
    }

    public Integer getPressure() {
        return pressure;
    }

    public WeatherEntity setPressure(Integer pressure) {
        this.pressure = pressure;
        return this;
    }

    public Integer getHumidity() {
        return humidity;
    }

    public WeatherEntity setHumidity(Integer humidity) {
        this.humidity = humidity;
        return this;
    }

    public Double getWindSpeed() {
        return windSpeed;
    }

    public WeatherEntity setWindSpeed(Double windSpeed) {
        this.windSpeed = windSpeed;
        return this;
    }

    public Integer getWindDegrees() {
        return windDegrees;
    }

    public WeatherEntity setWindDegrees(Integer windDegrees) {
        this.windDegrees = windDegrees;
        return this;
    }

    public LocationEntity getLocationEntity() {
        return locationEntity;
    }

    public WeatherEntity setLocationEntity(LocationEntity locationEntity) {
        this.locationEntity = locationEntity;
        return this;
    }


}
