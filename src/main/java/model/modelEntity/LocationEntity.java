package model.modelEntity;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


@Entity //(name ="Locations")
public class LocationEntity {
    @Id
    @GeneratedValue
    private Long id;
    private String city;
    private double latitude;
    private double longitude;
    private String region;
    private String country;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "locationEntity", cascade = CascadeType.ALL)
    private List<WeatherEntity> weatherEntities = new ArrayList<>();


    public LocationEntity() {
    }

    public LocationEntity(String city, double latitude, double longitude, String region, String country) {
        this.city = city;
        this.latitude = latitude;
        this.longitude = longitude;
        this.region = region;
        this.country = country;
    }

    public LocationEntity(Long id, String city, double latitude, double longitude, String region, String country) {
        this.id = id;
        this.city = city;
        this.latitude = latitude;
        this.longitude = longitude;
        this.region = region;
        this.country = country;
    }

    public LocationEntity(Long id, String city, double latitude, double longitude, String region, String country, List<WeatherEntity> weatherEntities) {
        this.id = id;
        this.city = city;
        this.latitude = latitude;
        this.longitude = longitude;
        this.region = region;
        this.country = country;
        this.weatherEntities = weatherEntities;
    }

    public Long getId() {
        return id;
    }

    public LocationEntity setId(Long id) {
        this.id = id;
        return this;
    }

    public String getCity() {
        return city;
    }

    public LocationEntity setCity(String city) {
        this.city = city;
        return this;
    }

    public double getLatitude() {
        return latitude;
    }

    public LocationEntity setLatitude(double latitude) {
        this.latitude = latitude;
        return this;
    }

    public double getLongitude() {
        return longitude;
    }

    public LocationEntity setLongitude(double longitude) {
        this.longitude = longitude;
        return this;
    }

    public String getRegion() {
        return region;
    }

    public LocationEntity setRegion(String region) {
        this.region = region;
        return this;
    }

    public String getCountry() {
        return country;
    }

    public LocationEntity setCountry(String country) {
        this.country = country;
        return this;
    }

    public List<WeatherEntity> getWeatherEntities() {
        return weatherEntities;
    }

    public LocationEntity setWeatherEntities(List<WeatherEntity> weatherEntities) {
        this.weatherEntities = weatherEntities;
        return this;
    }

    public void addWeather(WeatherEntity weatherEntity) {
        weatherEntities.add(weatherEntity);
        weatherEntity.setLocationEntity(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LocationEntity that = (LocationEntity) o;
        return Double.compare(that.latitude, latitude) == 0 && Double.compare(that.longitude, longitude) == 0 && id.equals(that.id) && city.equals(that.city) && Objects.equals(region, that.region) && Objects.equals(country, that.country);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, city);
    }
}
