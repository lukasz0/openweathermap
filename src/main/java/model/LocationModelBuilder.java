package model;

import java.util.List;

public class LocationModelBuilder {
    private Long id;
    private String city;
    private double latitude;
    private double longitude;
    private String region;
    private String country;
    private List<WeatherModel> weatherModels;

    public LocationModelBuilder setId(Long id) {
        this.id = id;
        return this;
    }

    public LocationModelBuilder setCity(String city) {
        this.city = city;
        return this;
    }

    public LocationModelBuilder setLatitude(double latitude) {
        this.latitude = latitude;
        return this;
    }

    public LocationModelBuilder setLongitude(double longitude) {
        this.longitude = longitude;
        return this;
    }

    public LocationModelBuilder setRegion(String region) {
        this.region = region;
        return this;
    }

    public LocationModelBuilder setCountry(String country) {
        this.country = country;
        return this;
    }

    public LocationModelBuilder setWeatherModels(List<WeatherModel> weatherModels) {
        this.weatherModels = weatherModels;
        return this;
    }

    public LocationModel createLocationModel() {
        return new LocationModel(id, city, latitude, longitude, region, country, weatherModels);
    }
}