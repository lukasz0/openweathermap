package model;

public class WeatherModelBuilder {
    private Long idWeather;
    private double temperature;
    private Integer pressure;
    private Integer humidity;
    private Double windSpeed;
    private Integer windDegrees;
    private LocationModel locationModel;

    public WeatherModelBuilder setIdWeather(Long idWeather) {
        this.idWeather = idWeather;
        return this;
    }

    public WeatherModelBuilder setTemperature(double temperature) {
        this.temperature = temperature;
        return this;
    }

    public WeatherModelBuilder setPressure(Integer pressure) {
        this.pressure = pressure;
        return this;
    }

    public WeatherModelBuilder setHumidity(Integer humidity) {
        this.humidity = humidity;
        return this;
    }

    public WeatherModelBuilder setWindSpeed(Double windSpeed) {
        this.windSpeed = windSpeed;
        return this;
    }

    public WeatherModelBuilder setWindDegrees(Integer windDegrees) {
        this.windDegrees = windDegrees;
        return this;
    }

    public WeatherModelBuilder setLocationModel(LocationModel locationModel) {
        this.locationModel = locationModel;
        return this;
    }

    public WeatherModel createWeatherModel() {
        return new WeatherModel(idWeather, temperature, pressure, humidity, windSpeed, windDegrees, locationModel);
    }
}