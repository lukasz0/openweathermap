package Conroller;

import model.*;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class WeathermanController {


    private final WeathermanService weathermanService = new WeathermanService();
    private final Mapper mapper = new Mapper();
    public static final Logger LOGGER = Logger.getLogger(WeathermanController.class.getName());


    //C-create
    //tworzy lokalizację
    public LocationModel createLocation(WeatherRequestModel weatherRequestModel) {

//      BRAK SPRAWDZANIA CZY DANE SA POPRAWNE (czy klasa weatherRequestModel nie jest pusta), gdyż to zostanie sprawdzone w WeathermanCli

//      WeatherRequestMapper->LocationModel locationModel
        LocationModel locationModel = mapper.mapperFromWeatherRequestToLocationModel(weatherRequestModel);

        LOGGER.info("Wejściowy: " + locationModel);
        LocationModel locationModelOut = weathermanService.createLocation(locationModel);
        LOGGER.info("Z bazy danych: " + locationModelOut);
        return locationModelOut;
    }


    // tworzy lokalizację (jeżeli nie istnieje) i pogodę
    public WeatherModel createLocationWithWeather(WeatherRequestModel weatherRequestModel) {

        LocationModel locationModel = mapper.mapperFromWeatherRequestToLocationModel(weatherRequestModel);
        WeatherModel weatherModelOut = weathermanService.createWeatherWithLocation(locationModel);
        return weatherModelOut;

    }

    //tworzy pogodę dla istniejącej lokalizacji
    public LocationModel createWeatherForLocation(WeatherRequestModel weatherRequestModel) {
        LocationModel locationModel = mapper.mapperFromWeatherRequestToLocationModel(weatherRequestModel);
        LocationModel locationModelOut = weathermanService.createWeatherForLocation(locationModel);
        return locationModelOut;
    }

    //R -read

    public LocationModel read(WeatherRequestModel weatherRequestModel) {

        LocationModel locationModel = mapper.mapperFromWeatherRequestToLocationModel(weatherRequestModel);
        LocationModel locationModelOut = weathermanService.read(locationModel);
        //LOGGER.info("locationModelOut From DB: " + locationModelOut);
        return locationModelOut;
    }

    public LocationModel readLastWeather(WeatherRequestModel weatherRequestModel) {
        LocationModel locationModel = mapper.mapperFromWeatherRequestToLocationModel(weatherRequestModel);
        LocationModel locationModelOut = weathermanService.read(locationModel);
        List<WeatherModel> weatherModel=new ArrayList<>();
        weatherModel.add(locationModelOut.getWeatherModels().get(locationModelOut.getWeatherModels().size()-1));
        locationModelOut.setWeatherModels(weatherModel);
        return locationModelOut;
    }

    //U - update

    public LocationModel updateLocationById(Long idLocation, WeatherRequestModel newWeatherRequestModel) {
        LocationModel locationModel = mapper.mapperFromWeatherRequestToLocationModel(newWeatherRequestModel);
        LocationModel locationModelOut = weathermanService.updateLocationService(idLocation, locationModel);
        LOGGER.info("locationModelOut From DB: " + locationModelOut);
        return locationModelOut;
    }

    //D-delete

    public LocationModel deleteLocation(WeatherRequestModel weatherRequestModel) {
        LocationModel locationModel = mapper.mapperFromWeatherRequestToLocationModel(weatherRequestModel);
        LocationModel locationModelOut = weathermanService.deleteLocationService(locationModel);
        return locationModelOut;
    }

    public LocationModel deleteLocationById(Long idLocation) {

        LocationModel locationModelOut = weathermanService.deleteLocationByIdService(idLocation);
        return locationModelOut;
    }


    //L

    public List<LocationModel> listLocation() {
        List<LocationModel> locationModels = weathermanService.getLocationModels();
        if (locationModels.isEmpty()){
            System.out.println("Brak wpisów \n");
        }
        return locationModels;
    }


}
