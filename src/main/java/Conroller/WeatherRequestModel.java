package Conroller;

public class WeatherRequestModel {
    private String city;
    private String countryCode;

    public WeatherRequestModel(String city, String countryCode) {
        this.city = city;
        this.countryCode = countryCode;
    }

    public String getCity() {
        return city;
    }

    public WeatherRequestModel setCity(String city) {
        this.city = city;
        return this;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public WeatherRequestModel setCountryCode(String countryCode) {
        this.countryCode = countryCode;
        return this;
    }
}
