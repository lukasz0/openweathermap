package openweather;

import dao.openweather.Model.WeatherFromServerDataResponse;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WeatherModelFromServerDataResponseTest {
    @Test
    public void getWeatherTest() {
        // given
        WeatherFromServerDataResponse weatherFromServerDataResponse = new WeatherFromServerDataResponse();

        //when
        String currentWeatherString = weatherFromServerDataResponse.getWeather();

        //then
        assertNotNull(currentWeatherString, "currentWeatherString is null - no weather data");
    }
}