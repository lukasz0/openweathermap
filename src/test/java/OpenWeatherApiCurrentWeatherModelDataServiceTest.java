import dao.openweather.Model.OpenWeatherApiCurrentWeatherDataResponse;
import dao.openweather.Service.OpenWeatherApiCurrentWeatherDataService;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertNotNull;

//od prowadzącego


class OpenWeatherApiCurrentWeatherModelDataServiceTest {

    @Test
    void givenService_whenGetCurrentWeather_thenResponseIsNotNull() {
        // given
        OpenWeatherApiCurrentWeatherDataService service = new OpenWeatherApiCurrentWeatherDataService();

        // when
        OpenWeatherApiCurrentWeatherDataResponse weatherDataResponse = service.getCurrentWeather("Warsaw");

        // then
        assertNotNull(weatherDataResponse, "weatherDataResponse is null - no weather data");
    }
}