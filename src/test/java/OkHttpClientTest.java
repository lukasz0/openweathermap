import okhttp3.*;
import org.junit.jupiter.api.Test;


import java.io.IOException;


import static org.junit.jupiter.api.Assertions.assertEquals;

public class OkHttpClientTest {
    public static final String WEATHER_URL = "http://api.openweathermap.org/data/2.5/weather";
    OkHttpClient client = new OkHttpClient();


    @Test
    public void whenGetRequestWithQueryParameter_thenCorrect()
            throws IOException {

        //przygotowanie URL z parametrami
        HttpUrl.Builder urlBuilder
                = HttpUrl.parse(WEATHER_URL).newBuilder();
        urlBuilder.addQueryParameter("q","Warsaw");
        urlBuilder.addQueryParameter("appid","8296fc0b0a4a4c127a6bf58432026042");

        String url = urlBuilder.build().toString();

        //przygotowanie żądania GET (jeżeli nie podamy, że to ma być Post to domyślnie jest GET)
        Request request = new Request.Builder()
                .url(url)
                .build();

        //wykonanie request
        Call call = client.newCall(request);
        Response response = call.execute();

        assertEquals(200,response.code());

       //assertThat(response.code(), equalTo(200));
    }
}
